module Main

import Prolude

main : IO ()
main = printLn ([0 .. 10] `contains` 3)
