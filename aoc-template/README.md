# AOC template

This is a project template for AOC making use of `Prolude` as a library to help
solve problems. The different modules are:

- Prolude: utilities for `Nat`, `List` and `Vect`.
- Prolude.Cryptography: utilities for dealing with cryptographic operations.
- Prolude.NGrid: A data type for n-dimensional grid layouts.
- Prolude.Queue: Queues and sized queues
- Prolude.SortedMap: utilities for Data.SortedMap
- Prolude.Stream: Utilities for Data.Stream
- Prolude.Stack: Stacks with pop and push
- Prolude.Binary: Binary vectors

## How to use this template

This assumes that you already have Idris2 installed and `rlwrap`. You will benefit from also installing LSP for Idris2.

1. clone the prolude project `git clone https://gitlab.com/avidela/prolude.git`
2. go in the project folder and run `idris2 --install prolude.ipkg`
3. go in `aoc-template` (this folder)
4. run `rlwrap idris2 --repl template.ipkg`

if that works you should be in a repl with `base`, `contrib` and `prolude` libraries
loaded and the `src/Main.idr` file loaded. You can start writing code in `src/Main.idr`
and compile it with `:r` in the repl.
