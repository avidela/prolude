module Prolude.Data.SortedMap

import Data.SortedMap
import Data.Maybe

||| given a key-value pair, add the value if the key is missing
export
insertIfMissing : Eq a => (a , b) -> SortedMap a b ->  SortedMap a b
insertIfMissing (key, value) map =
  case lookup key map of
       Nothing => insert key value map
       Just v => map

||| Update a key with the `update` function or insert the default value `def`
export
updateOrDefault : (key : k) -> (update : v -> v) -> (def : v) -> SortedMap k v  -> SortedMap k v
updateOrDefault key f def map =
  case lookup key map of
       Nothing => insert key def map
       Just v => insert key (f v) map

||| update the value at the given if it's present
export
update : key -> (updateFn : value -> value) -> SortedMap key value -> SortedMap key value
update key updateFn smap =
  let value = map updateFn $ lookup key smap in
      fromMaybe smap $ map (\v => insert key v smap) value
