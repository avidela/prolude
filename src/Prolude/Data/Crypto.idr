module Prolude.Data.Crypto

import Data.List

export
GCDExt : Integer -> Integer -> (Integer, Integer)
GCDExt a b = GCDHelper a b 1 0 0 1
  where
    GCDHelper : (oldR, newR, oldS, newS, oldT, newT : Integer) -> (Integer, Integer)
    GCDHelper oldR 0 oldS newS oldT newT = (oldS, oldT)
    GCDHelper oldR newR oldS newS oldT newT =
      let q = oldR `div` newR in
          GCDHelper newR (oldR - q * newR) newS (oldS - q * newS) newT (oldT - q * newT)

export
modInv : Integer -> Integer -> Integer
modInv a b = fst $ GCDExt a b

pmod : Integer -> Integer -> Integer
pmod x y = if x < 0 then (x + y) `pmod` y else x `mod` y

||| Chinese products
export
chinese2 : (as, ns : List Integer) -> Integer
chinese2 as ns =
  let p = product ns
      nis = map (div p) ns
      crtM = zipWith (modInv) nis ns
      s = sum (zipWith3 (\a, b, c => a * b * c) as crtM nis)
   in s `pmod` p
