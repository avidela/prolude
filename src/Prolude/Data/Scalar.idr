module Prolude.Data.Scalar

public export
interface Scalar (0 s : Type -> Type) a | s where
  constructor MkScalar
  scalar : a -> s a -> s a

||| Scale a functor by a given factor
export
Num a => Functor f => Scalar f a where
  scalar factor = map (factor *) 

