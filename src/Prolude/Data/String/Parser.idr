module Prolude.Data.String.Parser

import Data.List
import public Data.List1
import Data.String
import Data.String.Parser

export
parseAll : Parser a -> String -> Either String a
parseAll p = map fst . parse (p <* eos)

export
parseArray : Parser a -> String -> Either String (List a)
parseArray p = map fst . parse (commaSep (lexeme p) <* eos)

export
lines1 : String -> Maybe (List1 String)
lines1 = fromList . lines

export
parseList1 : String -> Maybe (List1 Nat)
parseList1 input = lines1 input >>= traverse parsePositive
