module Prolude.Data.Stream

import Data.Stream
import Data.List

export
maybeCycle : List a -> Maybe (Stream a)
maybeCycle [] = Nothing
maybeCycle (x :: xs) = Just $ cycle (x :: xs)

