||| Module defining operations in an n-dimensional space
module Prolude.Data.NGrid

import public Data.Vect
import Prolude.Data.Vect

-- n-dimensional cubes
public export
NGrid : (side : Nat) -> (depth : Nat) -> (content : Type) -> Type
NGrid s Z elem = elem
NGrid s (S d) elem = Vect s (NGrid s d elem)

public export
Coordinate : Nat -> Type
Coordinate d = Vect d Nat

export
indexN : Coordinate d -> NGrid s d a -> Maybe a
indexN [] x = Just x
indexN (idx :: xs) board =
 indexNat board idx >>= indexN xs

||| Creates an empty space with `n` dimensions
export
zero : (n : Nat) -> Coordinate n
zero n = replicate n 0

export
emptyCube : Monoid a => (size, dim : Nat) -> NGrid size dim a
emptyCube size 0 = neutral
emptyCube size (S k) = replicate size (emptyCube {a} size k)

||| Embeds a surface of dimension `n` into a space of dimension `n + 1` by making empty copies of
||| the surface and sandwiching the original surface until we reach the correct volume
||| 
|||          ╱╲  
|||    a:   ╱  ╲  
|||        ╱    ╲ 
|||        ╲    ╱
|||    b:  ╱╲  ╱╲ 
|||        ╲ ╲╱ ╱
|||        ╱╲  ╱╲ 
|||    c:  ╲ ╲╱ ╱ 
|||         ╲  ╱
|||          ╲╱
||| 
||| In the diagram above, we are embedding a 3x3 surface `b` into a 3x3x3 cube
||| by sandwiching it between an empty surface `a` and another empty surface `c`
||| of the same size.
||| The parameters `x` and `y` allow you to pick the number of layers above and 
||| below the original surface.
export
embed : Monoid a => (dim : Nat) -> {x, y : Nat} -> NGrid (x + S y) dim a -> NGrid (x + S y) (S dim) a
embed dim board = let postfix = Vect.replicate y (emptyCube (x + S y) dim)
                      prefixx = Vect.replicate x (emptyCube (x + S y) dim)
                   in prefixx ++ (board :: postfix)

