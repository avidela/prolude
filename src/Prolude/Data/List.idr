module Prolude.Data.List

import Data.List
import Data.List1
import Data.Vect

%default total

infixl 8 !!

export
at : List a -> Nat -> Maybe a
at [] _ = Nothing
at (x :: xs) Z = Just x
at (x :: xs) (S n) = at xs n

export
(!!) : List a -> Nat -> Maybe a
(!!) = at

export
enumerate : List a -> List (Nat, a)
enumerate ls = zip [0 .. (length ls)] ls

export
max : Ord a => (ls : List a) -> {auto check : NonEmpty ls} -> a
max [x] = x
max (x :: (y :: xs)) = Prelude.max x (List.max (y :: xs))

export
unionAll : Eq a => (List (List a)) -> List a
unionAll = nub . join

export
split' : (p : a -> Bool) -> List a -> List (List a)
split' p = forget . split p

export
half : List a -> (List a, List a)
half xs = let h = length xs `div` 2 in splitAt h xs

export
contains : Eq a => List a -> a -> Bool
contains xs e = isJust $ find (== e) xs

export
subList : List a -> (lo, hi : Nat) ->  List a
subList ls lo hi = take (hi `minus` lo) (drop lo ls)

export
dropLast : List a -> List a
dropLast xs = take (length xs `minus` 1) xs
