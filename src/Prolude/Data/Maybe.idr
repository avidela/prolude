module Prolude.Data.Maybe

import Decidable.Equality

public export
partialEq : Dec a -> Maybe a
partialEq (Yes prf) = Just prf
partialEq (No contra) = Nothing
