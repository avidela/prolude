module Prolude.Data.Pair

import Prolude.Data.Scalar

export
Num a => Scalar (\x => Pair x x) a where
  scalar x (a, b) = (x * a, x * b)

