module Prolude.Data.Fin

import public Data.Fin

export
safeAdd : {n : Nat} -> Fin n -> Int -> Maybe (Fin n)
safeAdd x y = integerToFin (cast x + cast y) n
