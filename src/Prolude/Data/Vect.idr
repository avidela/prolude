module Prolude.Data.Vect

import public Data.Vect
import Data.List

export
max : Ord a => (ls : Vect (S n) a) -> a
max [x] = x
max (x :: (y :: xs)) = Prelude.max x (Vect.max (y :: xs))

public export
sizedList : List a -> (n ** Vect n a)
sizedList xs = (length xs ** fromList xs)

export
enumerate : Vect n a -> Vect n (Fin n, a)
enumerate [] = []
enumerate (x :: xs) = (FZ, x) :: map (mapFst Fin.FS) (enumerate xs)

export
enumerated : Num i => Vect n a -> Vect n (i, a)
enumerated x = enumerateAcc x 0
  where
    enumerateAcc : Vect m a -> i -> Vect m (i, a)
    enumerateAcc [] x = []
    enumerateAcc (y :: xs) x = (x, y) :: enumerateAcc xs (x + 1)

export
sortBy : (a -> a -> Ordering) -> Vect n a -> Vect n a
sortBy f xs = believe_me $ Vect.fromList $ sortBy f (toList xs)

export
indexNat : Vect n a -> Nat -> Maybe a
indexNat [] k = Nothing
indexNat (x :: xs) 0 = Just x
indexNat (x :: xs) (S k) = indexNat xs k

||| Drop the last element of the vector, same as `init`
||| This is `init` in the standard library
export
dropLast : Vect (S n) a -> Vect n a
dropLast [_] = []
dropLast (x :: (y :: xs)) = x :: dropLast (y :: xs)

||| Drop the last `n` elements of a vector
export
dropLastN : (n : Nat) -> Vect (n + m) a -> Vect m a
dropLastN 0 xs = xs
dropLastN (S k) (xs) = dropLast (dropLastN k (rewrite sym $ plusSuccRightSucc k m in xs))

||| Zip each element of the vector with the next one
export
zipVectConsecutive : Vect (S n) a -> Vect n (a, a)
zipVectConsecutive xs = zip (dropLast xs) (tail xs)
