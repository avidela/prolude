module Prolude.Data.Zipper

import Data.List
import Data.Vect
import Prolude.Data.Queue

public export
interface ZipperAPI (0 z : Type -> Type) where
  moveRight : z a -> z a
  moveLeft : z a -> z a
  moveRight' : z a -> Maybe (z a)
  moveLeft': z a -> Maybe (z a)
  toList : z a -> List a

export
moveRightN : ZipperAPI z => Nat -> z a -> z a
moveRightN 0 x = x
moveRightN (S k) x = moveRight (moveRightN k x)

export
moveLeftN : ZipperAPI z => Nat -> z a -> z a
moveLeftN 0 x = x
moveLeftN (S k) x = moveLeft (moveLeftN k x)

public export
record Zipper (a : Type) where
  constructor MkZipper
  lefts : List a
  rights : List a

export
ZipperAPI Zipper where
  moveRight (MkZipper xs []) = MkZipper [] (reverse xs)
  moveRight (MkZipper xs (y :: ys)) = MkZipper (y :: xs) ys
  moveLeft (MkZipper [] xs) = MkZipper (reverse xs) []
  moveLeft (MkZipper (y :: ys) xs) = MkZipper ys (y :: xs)
  moveRight' (MkZipper xs []) = Nothing
  moveRight' (MkZipper xs (y :: ys)) = Just $ MkZipper (y :: xs) ys
  moveLeft' (MkZipper [] xs) = Nothing
  moveLeft' (MkZipper (y :: ys) xs) = Just $ MkZipper ys (y :: xs)
  toList (MkZipper xs ys) = reverse xs ++ ys

namespace Zipper
  export
  fromList : List a -> Zipper a
  fromList ls = MkZipper [] ls

||| A big zipper is a zipper with a vector in the middle and two lists on each side of it
public export
record BigZipper (n : Nat) (a : Type) where
  constructor MkBigZipper 
  prev : List a 
  curr : SizedQueue n a
  next : List a

export
{n : Nat} -> ZipperAPI (BigZipper n) where

  -- If we can't move right, we cycle around
  moveRight (MkBigZipper xs vs []) = MkBigZipper [] (reverse vs) (reverse xs)
  
  -- The case where `n` is empty is the same as if as if the BigZipper was a normal Zipper
  moveRight (MkBigZipper xs vs (y :: ys)) {n = 0} = MkBigZipper (y :: xs) vs ys

  --  ..., ys , y] [v1, v2, v3, ... , vm, vn ] [x, xs, ...
  --                       |
  --                       v
  --  ..., ys, y, v1] [v2, v3, ..., vm, vn, x ] [xs , ...
  -- In the case where `n` is not empty, we pop the front of the queue, then add it to the
  -- left list, then push at the back the top of the right list, and return the updated Zipper
  moveRight (MkBigZipper xs vs (y :: ys)) {n = (S k)} = 
    let (elem, vs') = popElemFront vs in 
        MkBigZipper (elem :: xs) (pushBack y vs') ys

  -- If we can't move left, we cycle around
  moveLeft (MkBigZipper [] vs xs) = MkBigZipper (reverse xs) vs []  

  -- The case where `n` is empty is the same as if as if the BigZipper was a normal Zipper
  moveLeft (MkBigZipper (y :: ys) vs xs) {n = 0} = MkBigZipper ys vs (y :: xs)

  --  ..., ys , y] [v1, v2, v3, ... , vm, vn ] [x, xs, ...
  --                       |
  --                       v
  --  ..., ys ] [y, v1, v2, v3, ..., vm ] [ vn, x, xs , ...
  -- In the case where `n` is not empty, we pop the back of the queue, then add it to the
  -- right list, then push at the front the right-side element, and return the updated Zipper
  moveLeft (MkBigZipper (y :: ys) vs xs) {n = (S k)} = 
    let (elem, vs') = popElemBack vs in 
        MkBigZipper ys (pushFront y vs') (elem :: xs)

  moveRight' (MkBigZipper xs vs []) = Nothing
  moveRight' (MkBigZipper xs vs (y :: ys)) {n = 0} = Just (MkBigZipper (y :: xs) vs ys)

  --  ..., ys , y] [v1, v2, v3, ... , vm, vn ] [x, xs, ...
  --                       |
  --                       v
  --  ..., ys, y, v1] [v2, v3, ..., vm, vn, x ] [xs , ...
  -- In the case where `n` is not empty, we pop the front of the queue, then add it to the
  -- left list, then push at the back the top of the right list, and return the updated Zipper
  moveRight' (MkBigZipper xs vs (y :: ys)) {n = (S k)} = 
    let (elem, vs') = popElemFront vs in 
        Just (MkBigZipper (elem :: xs) (pushBack y vs') ys)

  moveLeft' (MkBigZipper [] vs xs) = Nothing

  -- The case where `n` is empty is the same as if as if the BigZipper was a normal Zipper
  moveLeft' (MkBigZipper (y :: ys) vs xs) {n = 0} = Just $ MkBigZipper ys vs (y :: xs)

  --  ..., ys , y] [v1, v2, v3, ... , vm, vn ] [x, xs, ...
  --                       |
  --                       v
  --  ..., ys ] [y, v1, v2, v3, ..., vm ] [ vn, x, xs , ...
  -- In the case where `n` is not empty, we pop the back of the queue, then add it to the
  -- right list, then push at the front the right-side element, and return the updated Zipper
  moveLeft' (MkBigZipper (y :: ys) vs xs) {n = (S k)} = 
    let (elem, vs') = popElemBack vs in 
        Just $ MkBigZipper ys (pushFront y vs') (elem :: xs)

  toList (MkBigZipper xs vs ys) = ?asdapok

namespace BigZipper
  export
  fromList : {n : Nat} -> List a -> Maybe (BigZipper n a)
  fromList xs = pure $ MkBigZipper [] !(toSized n (fromList $ List.take n xs)) (List.drop n xs)
