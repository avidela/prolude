module Prolude.Data.Cast

export
Cast (Either b a) (Maybe a) where
  cast (Left _) = Nothing
  cast (Right v) = Just v
