module Prolude.Data.Binary

import Data.Vect

||| Convert a Boolean to a number
export
num : Num a => Bool -> a
num False = 0
num True = 1

export
toBinary : Int -> (size : Nat) -> Vect size Bool
toBinary x Z = []
toBinary x (S size) = let i : Int = cast (S size) in (x `mod` 2 == 1) :: toBinary (x `div` 2) size

binaryAcc : Nat -> Nat -> Vect n Bool -> Nat
binaryAcc acc n [] = acc
binaryAcc acc n (x :: xs) = binaryAcc ((2 `power` n) * num x + acc) (S n) xs

export
binaryToDec : Vect n Bool -> Nat
binaryToDec vs = binaryAcc 0 0 (reverse vs)

||| Generate the list of vectors of length `n` where the input vector indicates which positions are fixed
||| (Using Just) and which ones are free to be either `True` or `False` using `Nothing`
generateFloating : Vect n (Maybe Bool) -> List (Vect n Bool)
generateFloating [] = [[]]
generateFloating (Nothing :: xs) = let rec = generateFloating xs in
                                       map (True ::) rec ++ map (False ::) rec
generateFloating ((Just x) :: xs) = map (x ::) (generateFloating xs)

export
(&&) : Vect n Bool -> Vect n Bool -> Vect n Bool
(&&) = zipWith (\x, y => x && y)

export
(||) : Vect n Bool -> Vect n Bool -> Vect n Bool
(||) = zipWith (\x, y => x || y)

parameters {size : Nat}
  public export
  Binary : Type
  Binary = Vect size Bool

  ||| A mask with uninitialised bits
  public export
  Mask : Type
  Mask = Vect size (Maybe Bool)

  export
  generateMask : Mask -> Binary -> List Binary
  generateMask mask bin =
    let masked = zipWith (\b => map (|| b)) bin mask
     in generateFloating masked

