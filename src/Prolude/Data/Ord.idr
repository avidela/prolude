module Prolude.Data.Ord

export
inBounds : Ord a => (lower, upper : a) -> a -> Bool
inBounds lower upper i = lower <= i && i < upper

