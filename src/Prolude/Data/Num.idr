module Prolude.Data.Num

import Prolude.Data.Group
infixl 9 ^

factAcc : Eq a => Neg a => Num a => a -> a -> a
factAcc n acc = if n == 0 then acc else factAcc (n - 1) $ n * acc

||| Iterative definition of factorial.
export
factorial: Eq a => Neg a => Num a => a -> a
factorial n = factAcc n 1

||| Binomial coefficient
export
choose : Integer -> Integer -> Integer
choose n k = foldl (\acc, i => (acc * (n + 1 - i)) `div` i) 1 [1 .. k]

||| Power
(^) : Neg a => Ord a => Integral a => a -> a -> a
(^) x y = case compare y 0 of
               EQ => 1
               LT => 1 `div` powerPos x (y * (-1)) 
               GT => powerPos x y
  where
    powerPos : a -> a -> a
    powerPos x y = if y == 0 then 1 else x * (x ^ (y - 1))
