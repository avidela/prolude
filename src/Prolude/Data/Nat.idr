module Prolude.Data.Nat

import Decidable.Equality
import public Data.Nat.Exponentiation
import public Data.Nat
import Data.List

import Prolude.Proofs

export
even : Nat -> Bool
even Z = True
even (S n) = not (even n)

export
odd : Nat -> Bool
odd = not . even

||| Compute the boolean value of a binary vector
||| Here the list of Nats is supposed to contain only 0 or 1s
export
toNumber : List Nat -> Nat
toNumber binary = 
  sum $ zipWith (*) (reverse binary) (map (\x => 2 `pow` x) [0 .. (length binary)])

||| Check if the first two number sum up to the third one
public export
isSum : (a, b, c : Nat) -> Dec (a + b = c)
isSum 0 b c = decEq b c
isSum (S k) b c = case isSum k (S b) c of
                       No ctr => No (ctr . (\Refl => sym (plusSuccRightSucc _ _)))
                       Yes prf => Yes (plusSuccRightSucc k b `trans` prf)

||| apply a function `n` times to an initial value
export
repeat : Nat -> (a -> a) -> a -> a
repeat 0 f x = x
repeat (S k) f x = repeat k f (f x)

-- same with vect and lists
export
scalar : Num n => n -> (n, n) -> (n, n)
scalar k (a, b) = (k * a, k * b)
