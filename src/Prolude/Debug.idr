module Prolude.Debug

import Debug.Trace

export
traceValue : Show v => v -> v
traceValue val = trace (show val) val

export
traceMsg : Show v => String -> v -> v
traceMsg msg val = trace "\{msg}: \{show val}" val
