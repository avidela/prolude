module Prolude.Loop

partial export
iterate : (number : Nat) -> (update : a -> a) -> (initial : a) -> a
iterate 0 f x = x
iterate (S k) f x = (iterate k f (f x))

partial export
fix : Eq a => (update : a -> a) -> (initial : a) -> a
fix f oldValue = let newValue = f oldValue in
                     if oldValue == newValue then newValue
                                             else fix f newValue

