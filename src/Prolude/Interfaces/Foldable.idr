module Prolude.Interfaces.Foldable

public export
size : Foldable t => t a -> Nat
size = count (const True)

namespace PositiveInteger
  export
  minimum : Foldable t => t Integer -> Integer
  minimum = foldl min 0

  export
  maximum : Foldable t => t Integer -> Integer
  maximum = foldl max 0

namespace NegativeInteger
  export
  minimum : Foldable t => t Integer -> Integer
  minimum = foldl max 0

  export
  maximum : Foldable t => t Integer -> Integer
  maximum = foldl min 0

namespace Nat
  export
  minimum : Foldable t => t Nat -> Nat
  minimum = foldl min 0

  export
  maximum : Foldable t => t Nat -> Nat
  maximum = foldl max 0
