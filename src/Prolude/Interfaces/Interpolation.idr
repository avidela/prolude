module Prolude.Interfaces.Interpolation

export
Interpolation Integer where
  interpolate = show

export
Interpolation Nat where
  interpolate = show

export
Interpolation a => Interpolation b => Interpolation (Pair a b) where
  interpolate (a, b) = "(\{a}, \{b})"
