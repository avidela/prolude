module Prolude

import public Data.List
import public Data.Vect

import public Prolude.Data.Nat as Prolude
import public Prolude.Data.Fin as Prolude
import public Prolude.Data.Maybe as Prolude
import public Prolude.Data.Ord as Prolude
import public Prolude.Data.Range as Prolude
import public Prolude.Data.Num as Prolude
import public Prolude.Data.Cast as Prolude
import public Prolude.Data.Vect as Prolude
import public Prolude.Data.List as Prolude
import public Prolude.Data.Pair as Prolude
import public Prolude.Data.Scalar as Prolude
import public Prolude.Data.Group as Prolude
import public Prolude.Data.SortedMap as Prolude
import public Prolude.Proofs as Prolude
import public Prolude.Interfaces.Foldable as Prolude
import public Prolude.Interfaces.Interpolation as Prolude
import public Prolude.Debug as Prolude
import public Prolude.Loop as Prolude
